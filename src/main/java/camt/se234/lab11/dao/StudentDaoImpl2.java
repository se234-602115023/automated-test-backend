package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl2 implements StudentDao {
    List<Student> students;
    public StudentDaoImpl2(){
        students = new ArrayList<>();
        students.add(new Student("345","A","temp",2.77));
        students.add(new Student("456","B","tempura",3.71));
        students.add(new Student("789","C","temple",3.19));
        students.add(new Student("012","D","temperature",2.45));
        students.add(new Student("345","E","template",1.77));

    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
