package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.dao.StudentDaoImpl2;
import camt.se234.lab11.entity.Student;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {

    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void setup() {
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);

    }

    @Test
    public void testFindById() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new
                Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("456"),is(new
                Student("456","B","tempura",3.21)));
        assertThat(studentService.findStudentById("789"),is(new
                Student("789","C","temple",3.99)));
        assertThat(studentService.findStudentById("012"),is(new
                Student("012","D","temperature",1.85)));
    }

    @Test
    public void testGetAverageGpa() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),closeTo(2.74,0.01));
    }

    @Test
    public void testGetAverageGpa2() {
        StudentDaoImpl2 studentDao = new StudentDaoImpl2();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),closeTo(2.78,0.01));
    }

    @Test
    public void testWithMock(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    @Test
    public void testWithMock2(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("S111","John","Cena",2.11));
        mockStudents.add(new Student("S315","Johnathan","Joestar",2.32));
        mockStudents.add(new Student("S001","James","Bond",3.4));
        mockStudents.add(new Student("S448","Joel","Vargskeletor",3.55));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("S448"),is(new Student("S448","Joel","Vargskeletor",3.55)));
    }

    @Test
    public void testWithMock3GetAverageGpa(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("S111","John","Cena",2.11));
        mockStudents.add(new Student("S315","Johnathan","Joestar",2.32));
        mockStudents.add(new Student("S001","James","Bond",3.4));
        mockStudents.add(new Student("S448","Joel","Vargskeletor",3.55));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),closeTo(2.85,0.01));
    }

    @Test
    public void testWithMock4GetAverageGpa(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("S111","John","Cena",2.13));
        mockStudents.add(new Student("S315","Johnathan","Joestar",3.38));
        mockStudents.add(new Student("S001","James","Bond",3.49));
        mockStudents.add(new Student("S448","Joel","Vargskeletor",1.22));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),closeTo(2.56,0.01));
    }

    @Test
    public void testWithMock5GetAverageGpa(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("S111","John","Cena",3.13));
        mockStudents.add(new Student("S315","Johnathan","Joestar",1.38));
        mockStudents.add(new Student("S001","James","Bond",3.19));
        mockStudents.add(new Student("S448","Joel","Vargskeletor",2.01));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),closeTo(2.43,0.01));
    }

    @Test
    public void testByFindPartOfId() {
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22"),hasItems(new Student("223","C","temp",2.33),
                new Student("224","D","temp",2.33)));


    }

    @Test
    public void testByFindPartOfId2() {
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("S111","John","Cena",3.13));
        mockStudents.add(new Student("S315","Johnathan","Joestar",1.38));
        mockStudents.add(new Student("S001","James","Bond",3.19));
        mockStudents.add(new Student("S448","Joel","Vargskeletor",2.01));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("001"),hasItem(new Student("S001","James","Bond",3.19)));
        assertThat(studentService.findStudentByPartOfId("1"),hasItems(new Student("S111","John","Cena",3.13),
                new Student("S001","James","Bond",3.19)));


    }

    @Test (expected = NoDataException.class)
    public void testNoDataException() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test (expected = NoDataException.class)
    public void testNoDataException2() {
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("44"),nullValue());
    }

    @Test (expected = ArithmeticException.class)
    public void testArithmeticException() {
        List<Student> mockStudents= new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());




    }




}
