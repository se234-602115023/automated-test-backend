package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("456","B","tempura",3.21));
        students.add(new Student("789","C","temple",3.99));
        students.add(new Student("012","D","temperature",1.85));
        students.add(new Student("345","E","template",2.31));

    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
